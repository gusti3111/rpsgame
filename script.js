let playerScore = 0;
let computerScore = 0;
const playerScoreSpan = document.getElementById("player-score")
const comScoreSpan = document.getElementById("com-score")
const scoreBoard_div = document.querySelector(".score")
const result_p = document.querySelector(".result > p")
const batu_div = document.getElementById("b")
const kertas_div = document.getElementById("k")
const gunting_div = document.getElementById("g")
const batucom_div = document.getElementById("b-com")
const kertascom_div = document.getElementById("k-com")
const guntingcom_div = document.getElementById("g-com")
const disableClick = () =>{
    
    batucom_div.style.pointerEvents = 'none';
    guntingcom_div.style.pointerEvents = 'none';
    kertascom_div.style.pointerEvents= 'none'
}
const getcomChoice = () =>{
    const choices = ['b-com','k-com','g-com']
    const rand = Math.floor(Math.random()*3)
    bgPlayer(choices[rand])
    disableClick()
    console.log(choices[rand])
    return choices[rand]
}
const bgPlayer = (choice) =>{
    const selectedChoice = document.getElementById(choice);
    selectedChoice.style.transform = "scale(1.1)"
    selectedChoice.style.transition = "all 1.5s ease "
    selectedChoice.style.borderRadius = "70px";

}
const Convert = (letter) =>{
    if(letter === "b") return "Rock"
    if(letter === "k") return "Paper"
    if(letter === "g") return "Scissors"
    if(letter === "b-com") return "Rock Com"
    if(letter === "k-com") return "Paper Com"
    if(letter === "s-com") return "Scissors Com"


    return "Scissors Com"

}
let win =  (playerChoice,comChoice) =>{
    playerScore++
  playerScoreSpan.innerHTML = playerScore;
    comScoreSpan.innerHTML = computerScore;
    result_p.innerHTML = `${Convert(playerChoice)}  beats  ${Convert(comChoice)} You Win`
    console.log("YOU WIN")
}
let lose =  (playerChoice,comChoice) =>{
    computerScore++;
    playerScoreSpan.innerHTML = playerScore;
      comScoreSpan.innerHTML = computerScore;
      result_p.innerHTML = `${Convert(comChoice)}  beats  ${Convert(playerChoice)} You Losse`
    console.log("YOU LOSSE")
     
}
let draw =  (playerChoice,comChoice) =>{
    result_p.innerHTML = `${Convert(playerChoice)}  equal  ${Convert(comChoice)} Draw`
    console.log("draw")
}

const game = (playerChoice) =>{
    const comChoice  = getcomChoice();
    switch (playerChoice + comChoice) {
        case "bg-com":
        case "kb-com":
        case "gk-com":
          win(playerChoice,comChoice)
            break;
        case "bk-com":
        case "gb-com":
        case "kg-com":
            lose(playerChoice,comChoice)
            break;
        case "bb-com":
        case "kk-com":
        case "gg-com":
            draw(playerChoice,comChoice)
            break;

                

    }
   
 
}
const main = () =>{
    batu_div.addEventListener("click",function () {
     
        document.getElementById('b').style.borderRadius ="60px"
        game('b')

        
    })
    kertas_div.addEventListener("click",function () {
        
        document.getElementById('k').style.borderRadius ="50px"

        game('k')
       
        
    })
    gunting_div.addEventListener("click",function () {
      
        document.getElementById('g').style.borderRadius ="50px"
        game('g')
        
        
    })

}
main()